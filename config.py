"""Configuration object with all required constants of the project"""


import os


class Values:

    """Update the attributes to let the other scripts import them"""

    folder = './'

    # pickled dict with site counts for each gene, context and consequence type
    SITE_COUNTS_PATH = os.path.join(folder, 'consequences.pickle.gz')

    # triplet counts genome-wide
    TRI_COUNT_GENOME_PATH = os.path.join(folder, 'tri.counts.genome.tsv')

    # triplet counts exome-wide
    TRI_COUNT_EXOME_PATH = os.path.join(folder, 'tri.counts.exome.tsv')

    # set of signatures
    SIGNATURES_PATH = os.path.join(folder, 'signatures.cosmic.genome.tsv')