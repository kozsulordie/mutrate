source activate mutrate_env

MUTRATE_OUTPUT="./mutrate_output"
TEST_FLAG=$1  # --test

python compute_mutrate.py compute_mutrate --annotmuts ./TARGET_WGS_WT_US.annotmuts.gz \
                                          --genemuts ./TARGET_WGS_WT_US.genemuts.gz \
                                          --weights ./TARGET_WGS_WT_US.out.gz \
                                          --output ${MUTRATE_OUTPUT} \
					  ${TEST_FLAG}

python compute_mutrate.py normalization --mutrate_output_folder ${MUTRATE_OUTPUT} \
					--constants_path ./constants.json
