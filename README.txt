# created by ferran.muinos@irbbarcelona.org
# revised on: 20 february 2019


Goal:
====

mutRate is a tool to infer site-specific neutral somatic mutation rates in the exome 
using mutational data from a cohort

the applications of mutRate are manifold:

i)   to understand the variability of backgound mutation rates in the exome and compare mutation 
     rates given factors such as gene, samples and mutational context
ii)  to study the deviance between neutral mutational processes and positive selection
iii) to generate training datasets by drawing randomized mutations


Output:
======

by "mutation site" or "site" we deem a specific single base substitution at a specific position in the genome
each position in the genome admits 3 different sites 

the value returned by mutRate at site M for a sample S reads:
"conditioning on the mutations observed in the cohort, 
mutRate gives the expected number of mutations at site M in sample S".

the factors determining the output of the method are:

- sample where the mutation occurs
- gene where the mutation occurs
- tri-nucleotide context of the mutation

given the values for these factors, the mutation rate is defined.


Main Input:
==========

either WXS or WGS somatic mutations from a cohort of samples


Other inputs:
============

- set of reference transcripts 
- proportion of each triplet in the genome
- triplet count per gene -- relative to the reference transcripts
- mutation site count per gene, context and consequence-type -- relative to the reference transcripts


What the method does:
====================

- given: dNdScv output -- configured to run with the reference transcripts
- given: signature fitting against COSMIC
- get expected number of synonymous mutations for gene (dNdScv)
- spread this expectation across samples in proportion to the sample's (synonymous) burden
- using the mutational profile of the sample and the synonymous contexts of the gene,
  spread the sample-specific synonymous mutation rate to get a the mutation rate per 
  synonymous site that depends on the context alone
- we extend these estimates to the non-synonymous sites in a sensible way, i.e.,
  * the sites with same context as found in non-synonymous sites get the same rate
  * for contexts not occurring in the synoymous region get rate s.t. relative rate matches the 
    mutational profile assigned to the sample.
- divide the expectation by the sample's (synonymous) mutation burden



Remarks:
=======

by using the count regression method implemented by dNdScv, we are implicitly using regional 
chromatin modification to best fit the effect of the gene covariate on the background mutation


Dependencies:
============

the method resorts requires the outputs of dNdScv by Martincorena et al. [1]

the method requires the output of the signature fitting of the cohort data against COSMIC; 
we use the method deconstructSigs by Rosenthal et al. [2]


References:
==========

[1] Martincorena I, et al. (2017) Universal Patterns of Selection in Cancer and Somatic Tissues. Cell. 
http://www.cell.com/cell/fulltext/S0092-8674(17)31136-4

[2] Rosenthal R, et al. (2016) deconstructSigs: delineating mutational processes in single tumors 
distinguishes DNA repair deficiencies and patterns of carcinoma evolution. Genome Biology. 
https://doi.org/10.1186/s13059-016-0893-4
